module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["plugin:vue/essential", "@vue/standard"],
  rules: {
    // 'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    camelcase: "off", //驼峰法命名
    // 方法名后面不加空格
    "space-before-function-paren": ["error", "never"],
    // 语句强制分号结尾
    semi: ["error", "always"],
    quotes: ["error", "single"]
  },
  parserOptions: {
    parser: "babel-eslint"
  }
};
