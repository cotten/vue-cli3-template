function browserEnvironment() {
  if (/MicroMessenger/.test(window.navigator.userAgent)) {
    return 'wechat';
  } else if (/AlipayClient/.test(window.navigator.userAgent)) {
    return 'alipay';
  } else {
    return 'other';
  }
}
function isBottomSliderPhone() {
  if (typeof window !== 'undefined' && window) {
    return (
      /iphone/gi.test(window.navigator.userAgent) && window.screen.height >= 812
    );
  }
  return false;
}
const state = {
  phoneBottomSlider: isBottomSliderPhone(), // 是否是底部有一条的iphone机型
  browserEnvironment: browserEnvironment(), // 浏览器环境
  // 去授权离开页面记录用户操作
  afterAuthorizeEvent: {
    page: '',
    event: '',
    params: ''
  }
};

const mutations = {
  SET_EVENT: (state, afterAuthorizeEvent) => {
    state.afterAuthorizeEvent = afterAuthorizeEvent;
  }
};

const actions = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
