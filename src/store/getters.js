const getters = {
  token: state => state.user.token,
  userInfo: state => state.user.userInfo,
  currentDate: state => state.position.currentDate,
  longitude: state => state.position.longitude,
  latitude: state => state.position.latitude,
  cityId: state => state.position.cityId,
  browserEnvironment: state => state.app.browserEnvironment,
  afterAuthorizeEvent: state => state.app.afterAuthorizeEvent,
  phoneBottomSlider: state => state.app.phoneBottomSlider
};
export default getters;
