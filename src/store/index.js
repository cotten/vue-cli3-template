import Vue from 'vue';
import Vuex from 'vuex';
import getters from './getters';
import persistedState from 'vuex-persistedstate';

Vue.use(Vuex);

// https://webpack.js.org/guides/dependency-management/#requirecontext
const modulesFiles = require.context('./modules', true, /\.js$/);

// 以下代码为了不需要去单独引入./modules下的模块文件
// modules会是所有./modules的对象集合
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  // set './user.js' => 'user'
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1');
  const value = modulesFiles(modulePath);
  modules[moduleName] = value.default;
  return modules;
}, {});

const store = new Vuex.Store({
  modules,
  getters,
  plugins: [
    persistedState({
      paths: [
        // 'user.token',
        // 'user.userInfo',
        'position.currentDate',
        'position.longitude',
        'position.latitude',
        'position.cityId',
        'app.afterAuthorizeEvent'
      ]
    })
  ]
});

export default store;
