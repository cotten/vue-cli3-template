import request from '@/utils/request';
const config = require('@/variableConfig');

const {
  baseHost,
  accountBaseApi,
  wechatBaseApi,
  wechatAppId,
  wechatSecret
} = config(process.env.MY_ENV);
const accountFullPath = baseHost + accountBaseApi;
const wechatFullPath = baseHost + wechatBaseApi;

// 登录 用户名密码
export function loginByAccount(data) {
  return request({
    baseURL: accountFullPath,
    url: '/v1/passwd/login',
    method: 'post',
    params: data
  });
}

// 获取短信验证码
export function getMsgCode(data) {
  return request({
    baseURL: accountFullPath,
    url: `/v1/send/${data.phone}`,
    method: 'get'
  });
}

// 领取红包
export function getRegbag(data) {
  return request({
    url: `/receive/v1/store/lucky/${data.cardId}`,
    method: 'post',
    params: data
  });
}

// 领取消费奖励
export function getRewards(data) {
  return request({
    url: `/receive/v1/store/privilege/${data.planId}`,
    method: 'post',
    params: data
  });
}

// 是否领取成功
export function isGetSuccess(data) {
  return request({
    url: `/receive/v1/rabbit/key/${data.key}`,
    method: 'post'
  });
}

// 微信登录
export function wxLogin(data) {
  return request({
    baseURL: accountFullPath,
    url: '/wx/login',
    method: 'post',
    params: data
  });
}

// 支付宝登录
export function aliLogin(data) {
  return request({
    baseURL: accountFullPath,
    url: '/alipay/login',
    method: 'post',
    params: data
  });
}

// 微信信息与用户手机号绑定
export function wxBind(data) {
  return request({
    baseURL: accountFullPath,
    url: '/wx/bind',
    method: 'post',
    params: data
  });
}

// 支付宝信息与用户手机号绑定
export function aliBind(data) {
  return request({
    baseURL: accountFullPath,
    url: '/alipay/bind',
    method: 'post',
    params: data
  });
}

// 判断用户是否关注公众号
export function isFocusWx(data) {
  return request({
    baseURL: accountFullPath,
    url: `/wx/subscribe/${data.accountId}`,
    method: 'get',
    params: data
  });
}

// 获取微信公众号二维码
export function getWxQrcode(data) {
  return request({
    baseURL: wechatFullPath,
    url: '/wx/qrcode',
    method: 'get',
    params: {
      appId: wechatAppId,
      secret: wechatSecret,
      ...data
    }
  });
}
