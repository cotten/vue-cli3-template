const DEMO_ROUTERS = [{
  path: '/demo',
  name: 'Demo',
  meta: {
    title: 'demo'
  },
  component: () =>
    import(
      /* webpackChunkName: "demo" */
      '@/views/demo.vue'
    )
}];
export default DEMO_ROUTERS;
