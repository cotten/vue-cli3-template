/**
 * @desc 根据npm run xxx命令执行
 * 参考：https://www.webpackjs.com/api/cli/#%E7%8E%AF%E5%A2%83%E9%80%89%E9%A1%B9
 */
const config = myenv => {
  let configObj = {};
  switch (myenv) {
    case 'test':
      configObj = {
        baseHost: 'https://buyer.jncloud.top',
        buyerBaseApi: '/api/jnc-buyer-api',
        accountBaseApi: '/api/jnc-account-api',
        wechatBaseApi: '/api/jnc-wechat-api',
        wechatAppId: 'wx630bc1bc56cbd6ab',
        wechatSecret: '1aa99ebf51d8b775f038db80d3d99bb2',
        aliAppId: '2019092467810104'
      };
      break;
      // 预发布环境 pre，生产环境 prod
    case 'pre':
      configObj = {
        baseHost: 'https://buyer.jncloud.top',
        buyerBaseApi: '/api/jnc-buyer-api',
        accountBaseApi: '/api/jnc-account-api',
        wechatBaseApi: '/api/jnc-wechat-api',
        wechatAppId: 'wxc317f90310b77f1d',
        wechatSecret: '5e080d2a0475e15f690f258413fbc4fd',
        aliAppId: '2019092467810104'
      };
      break;
    case 'prod':
      configObj = {
        baseHost: 'https://buyer.kdtyun.com',
        buyerBaseApi: '/api/jnc-buyer-api',
        accountBaseApi: '/api/jnc-account-api',
        wechatBaseApi: '/api/jnc-wechat-api',
        wechatAppId: 'wxc317f90310b77f1d',
        wechatSecret: '5e080d2a0475e15f690f258413fbc4fd',
        aliAppId: '2019092467810104'
      };
      break;
      // 默认为开发/测试 环境
    default:
      configObj = {
        baseHost: 'https://buyer.jncloud.top',
        buyerBaseApi: '/api/jnc-buyer-api',
        accountBaseApi: '/api/jnc-account-api',
        wechatBaseApi: '/api/jnc-wechat-api',
        wechatAppId: 'wx630bc1bc56cbd6ab',
        wechatSecret: '1aa99ebf51d8b775f038db80d3d99bb2',
        aliAppId: '2019092467810104'
      };
      break;
  }
  return configObj;
};

module.exports = config;
