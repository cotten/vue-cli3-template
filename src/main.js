import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import { parseTime, awaitWrap } from './utils';
// 按需引入vant UI组件和插件
import './plugins/vant.config';

import VConsole from 'vconsole';
const config = require('./variableConfig');
if (!(process.env.NODE_ENV === 'production' && process.env.MY_ENV === 'prod')) {
  // eslint-disable-next-line no-new
  new VConsole();
}

const currentConfig = config(process.env.MY_ENV);
Vue.config.productionTip = false;
Vue.prototype.$parseTime = parseTime;
Vue.prototype.$awaitWrap = awaitWrap;
Vue.prototype.$config = currentConfig;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
