// 正则表达式
let rules = {
  name(str) {
    return /^[A-Za-z\d\u4e00-\u9fa5]{1,12}$/.test(str);
  },
  phone(str) {
    // 手机号
    return /^1[3456789][0-9]\d{8}$/.test(str);
  },
  password(str) {
    // 密码
    return /^[a-z0-9]{6,}/.test(str);
  },
  idCard(str) {
    // 身份证号
    var _num18 = /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/.test(
      str
    );
    var _num15 = /^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{2}[0-9Xx]$/.test(
      str
    );
    return _num18 || _num15;
  }
};
// 校验正则
export default function(str, type) {
  try {
    return rules[type](str);
  } catch (e) {
    console.log('没有这个类型的check，需在regRxp.js中添加该正则');
  }
}
