module.exports = {
  plugins: {
    'postcss-import': {},
    'postcss-url': {},
    'postcss-aspect-ratio-mini': {},
    'postcss-cssnext': {},
    'postcss-px-to-viewport': {
      viewportWidth: 375, // (Number) The width of the viewport.
      viewportHeight: 667, // (Number) The height of the viewport.
      unitPrecision: 3, // (Number) The decimal numbers to allow the REM units to grow to.
      viewportUnit: 'vw', // (String) Expected units.
      selectorBlackList: ['.ignore', '.hairlines'], // (Array) The selectors to ignore and leave as px.
      /*
       selectorBlackList
       如果 value 是 string，则检查 selector 是否包含字符串。
       ['body'] 将会匹配 .body-class
       如果 value 是 regexp，它会检查选择器是否与正则表达式匹配。
       [/^body$/] 将会匹配 body 并不是 .body
      */
      minPixelValue: 1, // (Number) Set the minimum pixel value to replace.
      mediaQuery: false, // (Boolean) Allow px to be converted in media queries.
      // 忽略需要转换的文件夹
      exclude: []
    },
    // 'postcss-viewport-units': {},
    cssnano: {
      'cssnano-preset-advanced': {
        zindex: false,
        autoprefixer: false
      }
      // preset: 'advanced',
      // autoprefixer: false,
      // zindex: false
    }
  }
}
